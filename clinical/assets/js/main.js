var investigatorToken = '';

$(document).ready(function() {

    /* ======= Twitter Bootstrap hover dropdown ======= */   
    /* Ref: https://github.com/CWSpear/bootstrap-hover-dropdown */ 
    /* apply dropdownHover to all elements with the data-hover="dropdown" attribute */
    
    
    if (typeof dropdownHover !== "undefined") {
        $('[data-hover="dropdown"]').dropdownHover();
    }
    
    /* ======= Fixed header when scrolled ======= */    
    $(window).on('scroll load', function() {
         
         if ($(window).scrollTop() > 0) {
             $('#header').addClass('scrolled');
			 $('#header-logo').attr('src', './assets/images/logos/logo_bw2.png')
         }
         else {
             $('#header').removeClass('scrolled');
             $('#header-logo').attr('src', './assets/images/logos/logo_bw1.png')
         }
    });
    
    /* ======= jQuery Placeholder ======= */
    /* Ref: https://github.com/mathiasbynens/jquery-placeholder */
    if (typeof dropdownHover !== "undefined") {
        $('input, textarea').placeholder();
    }
    
    /* ======= jQuery FitVids - Responsive Video ======= */
    /* Ref: https://github.com/davatron5000/FitVids.js/blob/master/README.md */
    if (typeof dropdownHover !== "undefined") {
        $(".video-container").fitVids();
    }
    
    /* ======= FAQ accordion ======= */
    function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find('.panel-title a')
        .toggleClass('active')
        .find("i.fa")
        .toggleClass('fa-plus-square fa-minus-square');
    }
    $('.panel').on('hidden.bs.collapse', toggleIcon);
    $('.panel').on('shown.bs.collapse', toggleIcon);    
    
    
    /* ======= Header Background Slideshow - Flexslider ======= */    
    /* Ref: https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties */
    if (typeof dropdownHover !== "undefined") {
        $('.bg-slider').flexslider({
            animation: "fade",
            directionNav: false, //remove the default direction-nav - https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
            controlNav: false, //remove the default control-nav
            slideshowSpeed: 8000
        });
    }
	
	/* ======= Stop Video Playing When Close the Modal Window ====== */
    $("#modal-video .close").on("click", function() {
        $("#modal-video iframe").attr("src", $("#modal-video iframe").attr("src"));        
    });
     
    
     /* ======= Testimonial Bootstrap Carousel ======= */
     /* Ref: http://getbootstrap.com/javascript/#carousel */
    $('#testimonials-carousel').carousel({
      interval: 8000 
    });
    
    
    /* ======= Style Switcher ======= */    
    $('#config-trigger').on('click', function(e) {
        var $panel = $('#config-panel');
        var panelVisible = $('#config-panel').is(':visible');
        if (panelVisible) {
            $panel.hide();          
        } else {
            $panel.show();
        }
        e.preventDefault();
    });
    
    $('#config-close').on('click', function(e) {
        e.preventDefault();
        $('#config-panel').hide();
    });
    
    
    $('#color-options a').on('click', function(e) { 
        var $styleSheet = $(this).attr('data-style');
		$('#theme-style').attr('href', $styleSheet);	
				
		var $listItem = $(this).closest('li');
		$listItem.addClass('active');
		$listItem.siblings().removeClass('active');
		
		e.preventDefault();
	});
    
    $('.contact_investigator').click(function(evt) {
        evt.preventDefault();
        investigatorToken = $(this).attr('token');
        displayMsgForm();
    });
    
    $('.get_premium_connect').click(function(evt) {
        evt.preventDefault();
        initPremiumConnect();
    });
    
    //
    $('.modify_fq_answer').click(function(evt) {
        evt.preventDefault();
        var btn_wrapper = $(this).parent();
        $(btn_wrapper).hide();
        $(btn_wrapper).next().show();
        
        var ans_wrapper = $(this).parent().parent().parent();
        var ans_wrap_id = $(ans_wrapper).attr('id');
        $('#'+ans_wrap_id+' input, #'+ans_wrap_id+' select, #'+ans_wrap_id+' textarea').removeAttr('disabled');
    });
    
    //
    $('.cancel_modified_fq_answer').click(function(evt) {
        evt.preventDefault();
        var btn_wrapper = $(this).parent();
        $(btn_wrapper).hide();
        $(btn_wrapper).prev().show();
        
        var ans_wrapper = $(this).parent().parent().parent();
        var ans_wrap_id = $(ans_wrapper).attr('id');
        
        $('#'+ans_wrap_id+' input, #'+ans_wrap_id+' select, #'+ans_wrap_id+' textarea').each(function() {
            $(this).val(function() {
                switch (this.type) {
                    case 'text':
                        return this.defaultValue;
                    case 'textarea':
                        return this.defaultValue;
                    case 'checkbox':
                    case 'radio':
                        this.checked = this.defaultChecked;
                }
            });
            $(this).attr('disabled', 'disabled');
        });
    });
    
    //
    $('.save_modified_fq_answer').click(function(evt) {
        evt.preventDefault();
        var value_to_submit = {};
        var json_items      = [];
        var btn_wrapper     = $(this).parent();
        var poll_id         = $('#poll_id').val();
        
        $(btn_wrapper).hide();
        
        var ans_wrapper = $(this).parent().parent().parent();
        var ans_wrap_id = $(ans_wrapper).attr('id');
        
        $('#'+ans_wrap_id+' input[type="text"], #'+ans_wrap_id+' input[type="radio"], #'+ans_wrap_id+' input[type="checkbox"], #'+ans_wrap_id+' select, #'+ans_wrap_id+' textarea').each(function() {
            $(this).attr('disabled', 'disabled');
            
            var q_id    = $('#'+ans_wrap_id+' input[type="hidden"]').val();
            var type = $(this).attr('type');
            
            if (type == 'checkbox' || type == 'radio') {
                if ($(this).is(':checked')) {
                    json_items.push({
                        poll_id : poll_id, 
                        q_id    : q_id, 
                        ans     : $(this).val()
                    });
                }
            } else {
                json_items.push({
                    poll_id : poll_id, 
                    q_id    : q_id, 
                    ans     : $(this).val()
                });
            }
        });
        
        submitFqAnswer(JSON.stringify(json_items));
    });
});


function submitFqAnswer(values)
{
    $.ajax({
        method: 'POST',
        url: 'action.php',
        data: {
            action : 'submitFqAnswer',
            token  : values
        },
        dataType: 'json'
    }).done(function(response) {
        if (response.status == '200') {
            alert(response.result);
        } else if (response.status == '401') {
            alert(response.result);
        }
    });
}

function displayMsgForm()
{
    var suffix = Date.now();
    
    $.ajax({
        method: 'GET',
        url: 'action.php',
        data: {
            action : 'msgForm',
            suffix : suffix,
            token  : investigatorToken
        },
        dataType: 'json'
    }).done(function(response) {
        if (response.status == '200') {
            
            $('body').append(response.result);
            $('#modal_'+suffix).modal('show');
            
            //handle log in
            handleLogIn(suffix);
            
            //handle displaying registration form
            handleRegistrationForm(suffix);
            
            //handle send message form events
            handleMsgForm(suffix);
            
            $('#modal_'+suffix).on('hidden.bs.modal', function() {
                $(this).data('bs.modal', null);
                $(this).remove();
            });
        } else if (response.status == '401') {
            alert(response.result);
        }
    });
}

function handleRegistrationForm(suffix)
{
    $('#register_'+suffix).click(function() {
        $('#modal_'+suffix).modal('hide');
        
        if ($('#callback_'+suffix).val() == 'initPremiumConnect') {
            newPremiumConnect();
        } else {
            $('#PopupDisplayUserRole').appendTo("body").modal('show');
        }
    });
}

function handleLogIn(suffix)
{
    $('#login_'+suffix).click(function(evt) {
        evt.preventDefault();
        logIn(suffix);
    });
}

function logIn(suffix)
{
    $.ajax({
        method   : 'POST',
        url      : 'action.php',
        dataType : 'json',
        data     : {
            action   : 'login',
            suffix   : suffix,
            user     : $('#user_email').val(),
            hash     : $('#user_hash').val(),
            callback : $('#callback_'+suffix).val()
        }
    }).done(function(response) {
        if (response.status == '200') {
            //displayMsgForm();
            self[response.callback]();
            $('#modal_'+suffix).modal('hide');
            return true;
        } else {
            alert('Login Failed!')
            return false;
        }
    });
}

function handleMsgForm(suffix)
{
    $('#send_msg_'+suffix).click(function(evt) {
        evt.preventDefault();
        sendMsg(suffix);
    });
}

function sendMsg(suffix)
{
    $.ajax({
        method   : 'POST',
        url      : 'action.php',
        dataType : 'json',
        data     : {
            action  : 'sendMessage',
            suffix  : suffix,
            subject : $('#modal_'+suffix+' #subject').val(),
            body    : $('#modal_'+suffix+' #body').val(),
            token   : investigatorToken
        }
    }).done(function(response) {
        if (response.status == '201') {
            alert(response.result);
            $('#modal_'+suffix).modal('hide');
            return true;
        } else if (response.status == '401') {
            $('#modal_'+suffix).modal('hide');
            displayMsgForm();
        } else {
            alert('Failed to send message!')
            return false;
        }
    });
}

function initPremiumConnect()
{
    var suffix = Date.now();
    
    $.ajax({
        method: 'GET',
        url: 'action.php',
        data: {
            action : 'initPremiumConnect',
            suffix : suffix,
            //token  : investigatorToken
        },
        dataType: 'json'
    }).done(function(response) {
        if (response.status == '200') {
            
            $('body').append(response.result);
            $('#modal_'+suffix).modal('show');
            
            //handle log in
            handleLogIn(suffix);
            
            //handle displaying registration form
            handleRegistrationForm(suffix);
            
            //handle send message form events
            handleMsgForm(suffix);
            
            $('#modal_'+suffix).on('hidden.bs.modal', function() {
                $(this).data('bs.modal', null);
                $(this).remove();
            });
        } else if (response.status == '401') {
            alert(response.result);
        }
    });
}

function newPremiumConnect()
{
    var suffix = Date.now();
    
    $.ajax({
        method: 'GET',
        url: 'action.php',
        data: {
            action : 'newPremiumConnect',
            suffix : suffix,
            //token  : investigatorToken
        },
        dataType: 'json'
    }).done(function(response) {
        if (response.status == '200') {
            
            $('body').append(response.result);
            $('#modal_'+suffix).modal('show');
            
            //handle log in
            //handleLogIn(suffix);
            
            //handle displaying registration form
            //handleRegistrationForm(suffix);
            
            //handle send message form events
            //handleMsgForm(suffix);
            
            $('#modal_'+suffix).on('hidden.bs.modal', function() {
                $(this).data('bs.modal', null);
                $(this).remove();
            });
        } else if (response.status == '401') {
            alert(response.result);
        }
    });
    //contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
}

var QueryString = function () {
    // This function is anonymous, is executed immediately and 
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0; i<vars.length; i++) {
        var pair = vars[i].split("=");
            // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();